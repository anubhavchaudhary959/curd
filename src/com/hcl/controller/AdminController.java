package com.hcl.controller;

import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminImpl;
import com.hcl.model.Admin;
import com.hcl.model.Company;

public class AdminController {
	int result;
	IAdminDao dao = new IAdminImpl();

	public int adminAuthentication(String uid, String password) {

		Admin admin = new Admin(uid, password);
		return dao.adminAuthentication(admin);

	}

	public List<Company> viewAllCompany() {
		return dao.viewAllCompany();
	}

	public int addCompany(String cname, int cid, String cemail, int per10, int per12, int deg) {
		Company com = new Company(cname, cid, cemail, per10, per12, deg);
		return dao.addCompany(com);
	}

	public int editCname(String cname, int cid) {
		Company com = new Company();
		com.setCname(cname);
		com.setCid(cid);
		return dao.addCompany(com);
	}

	public int editCnameCemail(String cname, String cemail, int cid) {
		Company com = new Company();
		com.setCname(cname);
		com.setCemail(cemail);
		com.setCid(cid);
		return dao.addCompany(com);
	}

	public int editPer1012degCnamecemail(int per10, int per12, int deg, String cname, String cemail, int cid) {
		Company com = new Company();
		com.setPer10(per10);
		com.setPer12(per12);
		com.setDeg(deg);
		com.setCname(cname);
		com.setCemail(cemail);
		com.setCid(cid);
		return dao.addCompany(com);
	}

	public int removeCompany(int cid) {
		Company com = new Company();
		com.setCid(cid);
		return dao.removeCompany(com);

	}

}
