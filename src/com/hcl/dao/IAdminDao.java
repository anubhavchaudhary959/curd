package com.hcl.dao;

import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Company;

public interface IAdminDao {
	public int adminAuthentication(Admin admin);

	public List<Company> viewAllCompany();

	public int addCompany(Company com);

	public int editCname(Company com);

	public int editCnameCemail(Company com);

	public int editPer1012degCnamecemail(Company com);

	public int removeCompany(Company com);
}
