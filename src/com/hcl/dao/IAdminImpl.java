package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.Company;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminImpl implements IAdminDao {
	PreparedStatement pst;
	ResultSet rs;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			
			pst = Db.getConnection().prepareStatement(Query.adminAuth);
			
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in admin authentication");
		} finally {
			try {
				pst.close();

				rs.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | NullPointerException | SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<Company> viewAllCompany() {
		List<Company> list = new ArrayList<Company>();
		try {
			pst = Db.getConnection().prepareStatement(Query.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
				Company com = new Company(rs.getString(1), rs.getInt(2), rs.getString(3), rs.getInt(4), rs.getInt(5),
						rs.getInt(6));
				list.add(com);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in viewAll company");
		} finally {

			try {
				Db.getConnection().close();
				rs.close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}

		}
		return list;
	}

	@Override
	public int addCompany(Company com) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addCompany);
			pst.setString(1, com.getCname());
			pst.setInt(2, com.getCid());
			pst.setString(3, com.getCemail());
			pst.setInt(4, com.getPer10());
			pst.setInt(5, com.getPer12());
			pst.setInt(6, com.getDeg());

			result = pst.executeUpdate();

		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in add Company");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int editCname(Company com) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editCname);
			pst.setString(1, com.getCname());
			pst.setInt(2, com.getCid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in cname");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int editCnameCemail(Company com) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editCnameCemail);
			pst.setString(1, com.getCname());
			pst.setString(2, com.getCemail());
			pst.setInt(3, com.getCid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in cname and cemail");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int editPer1012degCnamecemail(Company com) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editCnameCemail);
			pst.setInt(1, com.getPer10());
			pst.setInt(2, com.getPer12());
			pst.setInt(3, com.getDeg());
			pst.setString(4, com.getCname());
			pst.setString(5, com.getCemail());
			pst.setInt(6, com.getCid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur in per1012deg cname and cemail");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;
	}

	@Override
	public int removeCompany(Company com) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.removeCompany);

			pst.setInt(1, com.getCid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occur remove company");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {

			}
		}
		return result;

	}
}
