package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.Company;

public class Main {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the user id");
		String uid = sc.nextLine();
		System.out.println("Enter password");
		String password = sc.nextLine();
		AdminController controller = new AdminController();
		int result = 0;
		result = controller.adminAuthentication(uid, password);
		if (result > 0) {
			System.out.println("Hi " + uid + " Welcome to Admin Page");
			int cont = 0;
			do {
				System.out.println(" 1.Add company  2.Remove company  3.Update Company  4.view all company");
				int option = sc.nextInt();
				sc.nextLine();
				switch (option) {
				case 1:
					System.out.println("-------COMPANY PORTAL-------");
					System.out.println("Enter Company name");
					String cname = sc.nextLine();
					
					System.out.println("Enter Company E-mail");
					String cemail = sc.nextLine();
					System.out.println("Enter Company Id");
					int cid = sc.nextInt();
					System.out.println("Enter Eligibilty Criteria Percentages ---");
					System.out.println("Enter Percentage of 10");
					int per10 = sc.nextInt();
					System.out.println("Enter Percentage of 12");
					int per12 = sc.nextInt();
					System.out.println("Enter Percentage of Degree");
					int deg = sc.nextInt();
					result = controller.addCompany(cname, cid, cemail, per10, per12, deg);
					if (result > 0) {
						System.out.println(cid + " Adding sucessfully");
					}
					break;
				case 2:
					System.out.println("Enter Company Id");
					cid = sc.nextInt();
					result = controller.removeCompany(cid);
					System.out.println((result > 0) ? cid + " Remove successfully" : cid + " Remove not successfull");

					break;

				case 3:
					System.out.println("1)edit cname 2.)edit cnamecemail 3)edit Per1012degCnamecemail ");
					option = sc.nextInt();
					if (option == 1) {
						System.out.println("Enter Company Id");
						cid = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter Company Name");
						cname = sc.nextLine();
						result = controller.editCname(cname, cid);
						System.out.println(
								(result > 0) ? cid + "  updated successfully" : cid + " updated not successfull");
					} else if (option == 2) {
						System.out.println("Enter company id");
						cid = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter company name");
						cname = sc.nextLine();
						System.out.println("enter company email");
						cemail = sc.nextLine();
						result = controller.editCnameCemail(cname, cemail, cid);
						System.out.println(
								(result > 0) ? cid + " updated successfully" : cid + "updated not successfull");

					} else if (option == 3) {
						System.out.println("Enter company id");
						cid = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter company name");
						cname = sc.nextLine();
						System.out.println("Enter company email");
						cemail = sc.nextLine();
						System.out.println("Enter 10 percentage of eligibility criteria ");
						per10 = sc.nextInt();
						System.out.println("Enter 12 percentage of eligibility criteria ");
						per12 = sc.nextInt();
						System.out.println("Enter degree percentage of eligibility criteria ");
						deg = sc.nextInt();

						result = controller.editPer1012degCnamecemail(per10, per12, deg, cname, cemail, cid);
						System.out.println(
								(result > 0) ? cid + " updated successfully" : cid + "updated not successfull");

					} else {
						System.out.println("invalid edit option");
					}
					break;

				case 4:
					List<Company> list = controller.viewAllCompany();
					if (list.size() > 0) {
						System.out.println("cname,cid,cemail,per10,per12,deg");
						for (Company com : list) {
							System.out.println(com.getCname() + "," + com.getCid() + "," + com.getCemail() + ","
									+ com.getPer10() + "," + com.getPer12() + "," + com.getDeg());

						}
					} else {
						System.out.println("no record found");
					}
					break;
				default:
					System.out.println("invalid selection");
				}
				System.out.println("would u like to continue press 1");
				cont = sc.nextInt();
			} while (cont == 1);

		} else {
			System.out.println("user id or password incorrect");
		}
		System.out.println("done");
		sc.close();
	}
}
