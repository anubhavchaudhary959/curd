package com.hcl.model;

public class Company {
	private String cname;
	private int cid;
	private String cemail;
	private int per10;
	private int per12;
	private int deg;
	public Company() {
		// TODO Auto-generated constructor stub
	}
	public Company(String cname, int cid, String cemail, int per10, int per12, int deg) {
		super();
		this.cname = cname;
		this.cid = cid;
		this.cemail = cemail;
		this.per10 = per10;
		this.per12 = per12;
		this.deg = deg;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCemail() {
		return cemail;
	}
	public void setCemail(String cemail) {
		this.cemail = cemail;
	}
	public int getPer10() {
		return per10;
	}
	public void setPer10(int per10) {
		this.per10 = per10;
	}
	public int getPer12() {
		return per12;
	}
	public void setPer12(int per12) {
		this.per12 = per12;
	}
	public int getDeg() {
		return deg;
	}
	public void setDeg(int deg) {
		this.deg = deg;
	}
	

}
